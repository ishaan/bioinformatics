//package org.example;
//
//import org.jgrapht.Graph;
//import org.jgrapht.Graphs;
//import org.jgrapht.graph.DefaultEdge;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.HashSet;
//
//public class Leiden {
//
//    private HashMap<Node, Community> map;
//    private Graph<Node, DefaultEdge> graph;
//
//    private Help help;
//
//    public Leiden(Graph<Node, DefaultEdge> g, HashMap<Node, Community> m, Help help){
//        graph = g;
//        map = m;
//        help = help;
//    }
//
//    public ArrayList<Community> MoveNodesFast(ArrayList<Community> list, float gamma){
//
//        float hOld = 0f;
//
//        HashSet<Node> nodes = new HashSet<>(graph.vertexSet());
//
//        for (Node v : nodes){
//
//            nodes.remove(v);
//            float deltaH = help.changeModularity(v, map.get(v), gamma);
//
//            Community c = map.get(v);
//
//            for (int i = 0; i < list.size(); i ++){
//                float comp = help.changeModularity(v, list.get(i), gamma);
//                if (comp > deltaH){
//                    c = list.get(i);
//                    deltaH = comp;
//                }
//            }
//            if (deltaH > 0){
//                map.get(v).getSet().remove(v);
//                map.put(v, c);
//                c.getSet().add(v);
//                for (Node n: Graphs.neighborSetOf(graph, v)){
//                    if (!c.getSet().contains(n)){
//                        if (!nodes.contains(n)){
//                            nodes.add(n);
//                        }
//                    }
//                }
//            }
//        }
//
//        return list;
//    }
//
//    public HashSet<Community> refinePart(ArrayList<Community> com){
//
//        Help temp = new Help(graph);
//
//        HashSet<Community> refined = temp.initComm();
//
//        for (Community c : com){
//            refined = mergeNodeSub();
//        }
//
//        return refined;
//    }
//
//    public HashSet<Community> mergeNodeSub(){
//
//        return null;
//    }
//
//}
