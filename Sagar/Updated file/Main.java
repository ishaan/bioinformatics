package org.example;

import org.jgrapht.*;
import org.jgrapht.graph.*;
import org.jgrapht.traverse.*;


import java.io.*;
import java.net.*;
import java.util.*;


public class Main {
    public static void main(String[] args) throws FileNotFoundException {


//        String test = "community 21246 = {11837, 6125, 11835, 9976, 9977, 11836, 11838, 11840, 11839, 6126, 11841}";
//
//        test = test.replaceAll("\\{", "");
//        test = test.replaceAll("\\}", "");
//        test = test.replaceAll(",", "");
//
//        System.out.println(test);


        HashMap<String, Node> nodes = new HashMap<String, Node>();

        HashMap<String, Integer> nums = new HashMap<String, Integer>();
        HashMap<Integer, String> revNums = new HashMap<Integer, String>();

        HashSet<Integer> ent = new HashSet<Integer>();

        String filename = "pathlinker-human-network.txt";
        File input = new File(filename);
        Scanner scan = new Scanner((input));
        String line = "";
        Graph<Node, DefaultEdge> graph = new DefaultUndirectedGraph<>(DefaultEdge.class);

        Integer in = 0;

        RandomAccessFile raf;
        String fileName = "walktrap_input.txt";
        File file = new File(fileName);
        raf = new RandomAccessFile(file, "rw");
        int m = 1;
        try {
            raf.setLength(0);
        } catch (IOException e) {
            e.printStackTrace();
        }

        while (scan.hasNext()) {
            line = scan.nextLine();
            if (!line.isEmpty()) {
                line = line.trim();
                String[] values = line.split("\\s+");
                if ((line.charAt(0) != '#') && values.length > 1) {
                    Node temp1 = nodes.get(values[0]);
                    Node temp2 = nodes.get(values[1]);
                    Integer n1 = nums.get(values[0]);
                    Integer n2 = nums.get(values[1]);

                    if (temp1 == null) {
                        temp1 = new Node(values[0]);
                        nodes.put(values[0], temp1);
                        if (ent.contains(in)){
                            in = in + 1;
                        }
                        n1 = in;
                        ent.add(in);
                        nums.put(values[0], n1);
                        revNums.put(n1, values[0]);
                        graph.addVertex(temp1);
                    }
                    if (temp2 == null) {
                        temp2 = new Node(values[1]);
                        if (ent.contains(in)){
                            in = in + 1;
                        }
                        n2 = in;
                        ent.add(in);
                        nodes.put(values[1], temp2);
                        nums.put(values[1], n2);
                        revNums.put(n2, values[1]);
                        graph.addVertex(temp2);
                    }
                    if (!graph.containsEdge(temp1, temp2)) {
                        graph.addEdge(temp1, temp2);

                        if (n1 == null || n2 == null){
                            System.out.println("Null number detected");
                        }

                        try {
                            String temp = n1 + " " + n2 + "\n";
                            raf.writeBytes(temp);
                        }
                        catch (IOException e){
                            e.printStackTrace();
                        }

                    }
                }
            }
        }
        scan.close();
        try {
            raf.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }


        //HashMap<String, List<String>> comList = new HashMap<String, List<String>>();

//        filename = "walking_communities_2.txt";
//        input = new File(filename);
//        scan = new Scanner((input));
//        line = "";
//
//        fileName = "walktrap_out2.txt";
//        file = new File(fileName);
//        raf = new RandomAccessFile(file, "rw");
//        m = 1;
//        try {
//            raf.setLength(0);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        while (scan.hasNext()) {
//            line = scan.nextLine();
//            if (!line.isEmpty()) {
//                line = line.trim();
//                line = line.replaceAll("\\{", "");
//                line = line.replaceAll("\\}", "");
//                line = line.replaceAll("\\,", "");
//                String[] values = line.split("\\s+");
//                if (values.length > 3 && line.charAt(0) == 'c'){
//                    try {
//                        String temp = "\nCommunity " + m + ": ";
//                        raf.writeBytes(temp);
//                        for (int j = 3; j < values.length; j++){
//                            String node = revNums.get(Integer.parseInt(values[j])) + " ";
//                            if (node == null){
//                                System.out.println("Was null");
//                            }
//                            raf.writeBytes(node);
//                        }
//                        m++;
//                    }
//                    catch (IOException e){
//                        e.printStackTrace();
//                    }
//                }
//
//            }
//        }
//
//        scan.close();
//        try {
//            raf.close();
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }

        filename = "walking_communities_4.txt";
        input = new File(filename);
        scan = new Scanner((input));
        line = "";

        fileName = "walktrap_out4.txt";
        file = new File(fileName);
        raf = new RandomAccessFile(file, "rw");
        m = 1;
        try {
            raf.setLength(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (scan.hasNext()) {
            line = scan.nextLine();
            if (!line.isEmpty()) {
                line = line.trim();
                line = line.replaceAll("\\{", "");
                line = line.replaceAll("\\}", "");
                line = line.replaceAll("\\,", "");
                String[] values = line.split("\\s+");
                if (values.length > 3 && line.charAt(0) == 'c'){
                    try {
                        String temp = "\nCommunity " + m + ": ";
                        raf.writeBytes(temp);
                        for (int j = 3; j < values.length; j++){
                            String node = revNums.get(Integer.parseInt(values[j])) + " ";
                            if (node == null){
                                System.out.println("Was null");
                            }
                            raf.writeBytes(node);
                        }
                        m++;
                    }
                    catch (IOException e){
                        e.printStackTrace();
                    }
                }

            }
        }

        scan.close();
        try {
            raf.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
//
//        filename = "walking_communities_50.txt";
//        input = new File(filename);
//        scan = new Scanner((input));
//        line = "";
//
//        fileName = "walktrap_out50.txt";
//        file = new File(fileName);
//        raf = new RandomAccessFile(file, "rw");
//        m = 1;
//        try {
//            raf.setLength(0);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        while (scan.hasNext()) {
//            line = scan.nextLine();
//            if (!line.isEmpty()) {
//                line = line.trim();
//                line = line.replaceAll("\\{", "");
//                line = line.replaceAll("\\}", "");
//                line = line.replaceAll("\\,", "");
//                String[] values = line.split("\\s+");
//                if (values.length > 3 && line.charAt(0) == 'c'){
//                    try {
//                        String temp = "\nCommunity " + m + ": ";
//                        raf.writeBytes(temp);
//                        for (int j = 3; j < values.length; j++){
//                            String node = revNums.get(Integer.parseInt(values[j])) + " ";
//                            if (node == null){
//                                System.out.println("Was null");
//                            }
//                            raf.writeBytes(node);
//                        }
//                        m++;
//                    }
//                    catch (IOException e){
//                        e.printStackTrace();
//                    }
//                }
//
//            }
//        }
//
//        scan.close();
//        try {
//            raf.close();
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }

//
//
//
//        Help help = new Help(graph);
//        HashSet<Community> set = help.initComm();
//        Louvain luv = new Louvain(graph, help, set);
//        System.out.println("PIN Gamma 100");
//        luv.funct(100f);
//        help.writeNodes(set, 100);



        //UndirectedModularityMeasurer(graph);

//
//
//        help = new Help(graph);
//        set = help.initComm();
//        luv = new Louvain(graph, help, set);
//        System.out.println("PIN Gamma 50");
//        luv.funct(50f);
//        help.writeNodes(set, 50);

//        help = new Help(graph);
//        set = help.initComm();
//        luv = new Louvain(graph, help, set);
//        System.out.println("PIN Gamma 10");
//        luv.funct(10f);
//
//        filename = "STRING-network.csv";
//        input = new File(filename);
//        scan = new Scanner((input));
//        line = "";
//        Graph<Node, DefaultEdge> graph2 = new DefaultUndirectedGraph<>(DefaultEdge.class);
//        int counter = 0;
//
//        while (scan.hasNext()) {
//            line = scan.nextLine();
//            if (!line.isEmpty()) {
//                line = line.replaceAll(",", " ");
//                line = line.trim();
//                String[] values = line.split("\\s+");
//                if ((counter != 0) && values.length > 1){
//                    Node temp1 = nodes.get(values[0]);
//                    Node temp2 = nodes.get(values[1]);
//                    if (temp1 == null){
//                        temp1 = new Node(values[0]);
//                        nodes.put(values[0], temp1);
//                        graph2.addVertex(temp1);
//                    }
//                    if (temp2 == null){
//                        temp2 = new Node(values[1]);
//                        nodes.put(values[1], temp2);
//                        graph2.addVertex(temp2);
//                    }
//                    if (!graph2.containsEdge(temp1, temp2)){
//                        graph2.addEdge(temp1, temp2);
//                    }
//                }
//            }
//            counter++;
//        }
//        scan.close();
//
//        help = new Help(graph2);
//        set = help.initComm();
//        luv = new Louvain(graph2, help, set);
//        System.out.println("STRING Gamma 0.1");
//        luv.funct(0.1f);
//
//        help = new Help(graph2);
//        set = help.initComm();
//        luv = new Louvain(graph2, help, set);
//        System.out.println("STRING Gamma 1");
//        luv.funct(1f);
//
//        help = new Help(graph2);
//        set = help.initComm();
//        luv = new Louvain(graph2, help, set);
//        System.out.println("STRING Gamma 10");
//        luv.funct(10f);

    }
}