package org.example;

import org.jgrapht.*;
import org.jgrapht.graph.*;
import org.jgrapht.traverse.*;

import java.io.*;
import java.net.*;
import java.util.*;


public class Main {
    public static void main(String[] args) throws FileNotFoundException {

        HashMap<String, Node> nodes = new HashMap<String, Node>();

        String filename = "pathlinker-human-network.txt";
        File input = new File(filename);
        Scanner scan = new Scanner((input));
        String line = "";
        Graph<Node, DefaultEdge> graph = new DefaultUndirectedGraph<>(DefaultEdge.class);

        while (scan.hasNext()) {
            line = scan.nextLine();
            if (!line.isEmpty()) {
                line = line.trim();
                String[] values = line.split("\\s+");
                if ((line.charAt(0) != '#') && values.length > 1){
                    Node temp1 = nodes.get(values[0]);
                    Node temp2 = nodes.get(values[1]);
                    if (temp1 == null){
                        temp1 = new Node(values[0]);
                        nodes.put(values[0], temp1);
                        graph.addVertex(temp1);
                    }
                    if (temp2 == null){
                        temp2 = new Node(values[1]);
                        nodes.put(values[1], temp2);
                        graph.addVertex(temp2);
                    }
                    if (!graph.containsEdge(temp1, temp2)){
                        graph.addEdge(temp1, temp2);
                    }
                }
            }
        }
        scan.close();

        Help help = new Help(graph);
        HashSet<Community> set = help.initComm();
        Louvain luv = new Louvain(graph, help, set);
        System.out.println("PIN Gamma 0.1");
        luv.funct(0.1f);
        help.writeNodes(set, 1);
//
//
//        help = new Help(graph);
//        set = help.initComm();
//        luv = new Louvain(graph, help, set);
//        System.out.println("PIN Gamma 1");
//        luv.funct(1f);
//
//        help = new Help(graph);
//        set = help.initComm();
//        luv = new Louvain(graph, help, set);
//        System.out.println("PIN Gamma 10");
//        luv.funct(10f);
//
//        filename = "STRING-network.csv";
//        input = new File(filename);
//        scan = new Scanner((input));
//        line = "";
//        Graph<Node, DefaultEdge> graph2 = new DefaultUndirectedGraph<>(DefaultEdge.class);
//        int counter = 0;
//
//        while (scan.hasNext()) {
//            line = scan.nextLine();
//            if (!line.isEmpty()) {
//                line = line.replaceAll(",", " ");
//                line = line.trim();
//                String[] values = line.split("\\s+");
//                if ((counter != 0) && values.length > 1){
//                    Node temp1 = nodes.get(values[0]);
//                    Node temp2 = nodes.get(values[1]);
//                    if (temp1 == null){
//                        temp1 = new Node(values[0]);
//                        nodes.put(values[0], temp1);
//                        graph2.addVertex(temp1);
//                    }
//                    if (temp2 == null){
//                        temp2 = new Node(values[1]);
//                        nodes.put(values[1], temp2);
//                        graph2.addVertex(temp2);
//                    }
//                    if (!graph2.containsEdge(temp1, temp2)){
//                        graph2.addEdge(temp1, temp2);
//                    }
//                }
//            }
//            counter++;
//        }
//        scan.close();
//
//        help = new Help(graph2);
//        set = help.initComm();
//        luv = new Louvain(graph2, help, set);
//        System.out.println("STRING Gamma 0.1");
//        luv.funct(0.1f);
//
//        help = new Help(graph2);
//        set = help.initComm();
//        luv = new Louvain(graph2, help, set);
//        System.out.println("STRING Gamma 1");
//        luv.funct(1f);
//
//        help = new Help(graph2);
//        set = help.initComm();
//        luv = new Louvain(graph2, help, set);
//        System.out.println("STRING Gamma 10");
//        luv.funct(10f);
//        Graph<Node, DefaultEdge> graph = new DefaultUndirectedGraph<>(DefaultEdge.class);
//        for (int i = 1; i < 28; i++){
//            Node n = new Node(Integer.toString(i));
//            graph.addVertex(n);
//        }



    }
}