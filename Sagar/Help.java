package org.example;

import org.jgrapht.Graph;
import org.jgrapht.graph.AsSubgraph;
import org.jgrapht.graph.DefaultEdge;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.*;

public class Help {

    private HashMap<Node, Community> map;
    private Graph<Node, DefaultEdge> g;

    public Help(Graph<Node, DefaultEdge> graph){
        map = new HashMap<Node, Community>();
        g = graph;
    }

    public float computeModularity(HashSet<Community> set, float gamma) {

        float m = (float)(g.edgeSet().size());

        float q = 0f;
        int counter = 0;

        for (Community c: set) {
            float d = 0f;
            float L = 1f;

            Set<Node> nodes = c.getSet();

            Graph<Node, DefaultEdge> sub = new AsSubgraph<>(g, nodes);

            L = (float)(sub.edgeSet().size());

            for (Node n : c.getSet()) {
                d += (float)(g.degreeOf(n));
            }
            //q += ((L / m) - gamma * (float)(Math.pow((d / (2f * m)), 2f)));
            q += (L / m) - gamma * ((d / (2f * m))) * ((d / (2f * m)));
            counter++;
        }
        return q;
    }

    public float changeModularity(Node l, Community j, float gamma) {
        float q = 0f;
        float m = (float)(g.edgeSet().size());

        float d = (float)(g.degreeOf(l));
        float k = 0f;
        float u = 0f;

        for (Node n : j.getSet()) {
            u += (float)(g.degreeOf(n));
            if (g.containsEdge(l, n)){
                k++;
            }
        }

        q = (float)(((1f / m) * k) - (gamma * (d / (2 * m * m)) * u));

        return q;
    }

    public HashSet<Community> initComm(){

        HashSet<Community> set = new HashSet<>();

        for (Node n : g.vertexSet()){
            Community c = new Community(n);
            map.put(n, c);
            set.add(c);
        }

        return set;
    }

    public HashMap<Node, Community> getMap(){
        return map;
    }

    public HashSet<Community> cleanComm(HashSet<Community> set) {
        HashSet<Community> s = new HashSet<Community>();
        for (Community c: set){
            if (!(c.getSet().isEmpty())){
                s.add(c);
            }
        }
        return s;
    }

    public void writeNodes(HashSet<Community> set, int i) throws FileNotFoundException {
        RandomAccessFile raf;
        String fileName = "Comms" + i + ".txt";
        File file = new File("Comm.txt");
        raf = new RandomAccessFile(file, "rw");
        int m = 1;

        try {
            raf.setLength(0);
            for (Community c: set){
                String temp = "Community " + i + ":\n";
                raf.writeBytes(temp);
                for (Node n: c.getSet()){
                    String node = n.getid() + "\n";
                    raf.writeBytes(node);
                }
                m++;
            }
            raf.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }

    }

}
