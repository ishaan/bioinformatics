package org.example;

import org.jgrapht.Graph;
import org.jgrapht.Graphs;
import org.jgrapht.graph.DefaultEdge;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Louvain {

    private HashMap<Node, Community> map;
    private Graph<Node, DefaultEdge> graph;

    private HashSet<Community> set;

    private Help help;

    public Louvain(Graph<Node, DefaultEdge> g, Help h, HashSet<Community> s){
        graph = g;
        help = h;
        map = help.getMap();
        set = s;

    }

    public void MoveNodes(float gamma)  {

        int counter = 0;

        for (Node v : graph.vertexSet()){
            counter++;
            float deltaH = help.changeModularity(v, map.get(v), gamma);

            System.out.println("Changed to itself = " + deltaH);

            HashSet<Community> tempSet = new HashSet<Community>();

            for (Node n: Graphs.neighborSetOf(graph, v)){
                if (!tempSet.contains(map.get(n))){
                    tempSet.add(map.get(n));
                }
            }

            Community c = map.get(v);
            boolean changed = false;

            for (Community q : tempSet){
                if (q != c){
                    float comp = help.changeModularity(v, q, gamma);
                    if (comp > deltaH){
                        c = q;
                        deltaH = comp;
                        changed = true;
                    }
                }
            }
            if ((deltaH > 0f) && changed){
                Community t = map.get(v);
                t.getSet().remove(v);
                map.put(v, c);
                c.getSet().add(v);
                if (t.getSet().isEmpty()){
                    set.remove(t);
                }
            }

        }

    }

    public void funct(float gamma) {
        boolean done = false;

        int counter = 1;

        while (!done) {

            float mod = help.computeModularity(set, gamma);

            System.out.println("First Modularity = " + mod);
            long startTime = System.currentTimeMillis();
            MoveNodes(gamma);
            long endTime = System.currentTimeMillis();
            done = true;
            for (Community c: set){
                if (!c.getSet().isEmpty()){
                    if (c.getSet().size() != 1){
                        done = false;
                    }
                }
            }
            float mad = help.computeModularity(set, gamma);

            System.out.println(counter + "\nModularity = " + mad);
            System.out.println("System time = " + (endTime - startTime));
            counter++;

            if ((mad - mod) <= 0.001f){
                done = true;
                System.out.println("Iteration done \n");
            }

        }
    }

}
