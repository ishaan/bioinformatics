package org.example;

import java.util.ArrayList;
import java.util.HashSet;

public class Community {

    private HashSet<Node> set;

    public Community(){
        set = new HashSet<Node>();
    }

    public Community(Node n){
        set = new HashSet<Node>();
        set.add(n);
    }

    public HashSet<Node> getSet() {
        return set;
    }

}
