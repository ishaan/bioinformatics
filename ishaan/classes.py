class Community:
    def __init__(self, index, initial_node):
        self.index = index
        self.set_of_nodes = {initial_node}

    # One instance method
    def add_node(self, node):
        self.set_of_nodes.add(node)

    # A second instance method
    def remove_node(self, node):
        self.set_of_nodes.remove(node)

    def community_size(self):
        return len(self.set_of_nodes)

    def is_empty_community(self):
        return len(self.set_of_nodes) == 0

    def get_nodes_in_community(self):
      return self.set_of_nodes

    def print_community(self):
      print(str(self.index) )
      print(list(self.set_of_nodes))

class Partition:
  def __init__(self, listcomm, index=10):
    self.index = index
    self.list_of_communities = []
    self.lookup = {}
    self.all_nodes = []
    for c in listcomm:
      self.list_of_communities.append(c)
      for n in c.set_of_nodes:
        self.lookup[n] = c.index
        self.all_nodes.append(n)

    # One instance method
  def move(self, node, new_community_index):
    self.list_of_communities[self.lookup[node]].remove_node(node)
    self.list_of_communities[new_community_index].add_node(node)
    self.lookup[node] = new_community_index

    # A second instance method
  def get_community_index(self, node):
    return self.lookup[node]

#need to do
  def get_community_from_node(self, node):
    return self.list_of_communities[self.lookup[node]]

  def number_of_nodes_in_partition(self):
    return len(self.lookup)

  def is_empty_partition(self):
    return len(self.lookup) == 0

  def get_nodes(self):
    return self.all_nodes

  def get_list_of_comm(self):
    return self.list_of_communities

def print_communities(P):
  for c in P.list_of_communities:
    print(c.set_of_nodes)
