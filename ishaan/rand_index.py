#!/usr/bin/env python3
import pandas as pd
import networkx as nx
import copy
import numpy as np
from itertools import combinations
from array import *
#Our Own Modules:
import cpm
from modularity import modularity, mod_change
from classes import Community, Partition
from commonfunctions import FindMaxLength, allSingleton, SingletonPartition, edge_set, print_communities

#READ + OPEN FILE
list_of_communities1 = []
with open("ishaan/Comms_1.txt", "r") as fp1:
  lines = fp1.readlines()
  index = 0
  for current_line in lines:
    args = current_line.split(" ")
    index = args[1][:-1]
    count = 0
    for n in args[2:-1]:
      if count == 0:
        temp_community = Community(index, n)
        count+=1
      else:
        temp_community.add_node(n)
    list_of_communities1.append(temp_community)

list_of_communities10 = []
with open("Comms_10.txt", "r") as fp10:
  lines = fp10.readlines()
  index = 0
  for current_line in lines:
    args = current_line.split(" ")
    index = args[1][:-1]
    count = 0
    for n in args[2:-1]:
      if count == 0:
        temp_community = Community(index, n)
        count+=1
      else:
        temp_community.add_node(n)
    list_of_communities10.append(temp_community)

#print("Communities in graph with gamma = 1")
Comm_1 = Partition(list_of_communities1)
#print_communities(Comm_1)

#print("Communities in graph with gamma = 10")
Comm_10 = Partition(list_of_communities10)
#print_communities(Comm_10)

def contingency_table_generator(P1,P2):
  X = P1.list_of_communities
  Y = P2.list_of_communities
  size_X = len(X)
  size_Y = len(Y)
  #print("X: " + str(size_X) + " Y: " + str(size_Y))
  table = [[0 for x in range (size_Y)] for y in range(size_X)]
  for X_i in X:
    for Y_j in Y:
      #print("community index X_i: " + X_i.index + " community nodes: " + str(X_i.set_of_nodes))
      #print("community index Y_j: " + Y_j.index + " community nodes: " + str(Y_j.set_of_nodes))
      nodes_in_X = X_i.set_of_nodes
      nodes_in_Y = Y_j.set_of_nodes
      common_nodes = nodes_in_X.intersection(nodes_in_Y)
      #print(common_nodes)
      n_ij = len(common_nodes)
      #print("hi")
      table[int(X_i.index)-1][int(Y_j.index)-1] = n_ij
  return table

contingency_table = contingency_table_generator(Comm_1,Comm_10)
arr = np.array(contingency_table)
#contingency_table = contingency_table_generator(Comm_1,Comm_10)
#print('\n'.join([' '.join([str(cell) for cell in row]) for row in contingency_table]))
#print(contingency_table)

rows = len(contingency_table)
cols = len(contingency_table[0])
#print(rows)
#print(cols)
X_sum = []
Y_sum = []
totalsum_a = 0
totalsum_b = 0
for i in range(0, rows):
    sumRow = 0
    for j in range(0, cols):
        sumRow = sumRow + contingency_table[i][j]
    X_sum.append(sumRow)
    totalsum_a+=sumRow
    #print("Sum of " + str(i+1) +" row: " + str(sumRow))

for i in range(0, cols):
    sumCol = 0;
    for j in range(0, rows):
        sumCol = sumCol + contingency_table[j][i]
    Y_sum.append(sumCol)
    totalsum_b+=sumCol
    #print("Sum of " + str(i+1) +" column: " + str(sumCol))

#Secondary check to make sure we have the same number of nodes in both - we do
print("total sum a: " + str(totalsum_a))
print("total sum b: " + str(totalsum_b))

#all variables we need for adjusted rand index calculation:
table = contingency_table
n = totalsum_a #same as totalsum_b
a = X_sum
b = Y_sum

#summations for i,j for (n_ij 2)
n_ij_2 = 0
#summation (n 2)
n_2 = n*(n-1)/2
for i in range(0,rows):
  for j in range(0,cols):
    n = contingency_table[i][j]
    n_ij_2+=(n*(n-1)/2)
    #print(str(contingency_table[i][j]) + " " + str(n_ij_2))
a_i_2 = 0
for a_i in a:
  a_i_2+=(a_i*(a_i-1)/2)
b_j_2 = 0
for b_j in b:
  b_j_2+=(b_j*(b_j-1)/2)

print("combination (n 2): " + str(n_2))
print("combination (n_ij 2): " + str(n_ij_2))
print("combination (a_i 2): " + str(a_i_2))
print("combination (b_j 2): " + str(b_j_2))
print()
ari_numerator = n_ij_2 - (a_i_2*b_j_2/n_2)
print("ARI numerator: " + str(ari_numerator))
ari_denominator = 0.5*(a_i_2+b_j_2) - (a_i_2*b_j_2/n_2)
print("ARI denominator: " + str(ari_denominator))
ARI = ari_numerator/ari_denominator
print("Adjusted Rand Index: " + str(ARI))