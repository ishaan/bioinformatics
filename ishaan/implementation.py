#!/usr/bin/env python3
import pandas as pd
import networkx as nx
import math
import copy
import numpy as np
human_network = open('human_protein_network.txt', 'r')
network = pd.read_table(human_network)
string_network = open('STRING_network.txt', 'r')
network2 = pd.read_table(string_network)

class Community:
    def __init__(self, index, initial_node):
        self.index = index
        self.set_of_nodes = {initial_node}

    # One instance method
    def add_node(self, node):
        self.set_of_nodes.add(node)
 
    # A second instance method
    def remove_node(self, node):
        self.set_of_nodes.remove(node)
    
    def community_size(self):
        return len(self.set_of_nodes)
      
    def is_empty_community(self):
        return len(self.set_of_nodes) == 0
    
    def get_nodes_in_community(self):
      return self.set_of_nodes
    
    def print_community(self):
      print(str(self.index) )
      print(list(self.set_of_nodes))
      
class Partition:
  def __init__(self, listcomm, index=10):
    self.index = index
    self.list_of_communities = []
    self.lookup = {}
    self.all_nodes = []
    for c in listcomm:
      self.list_of_communities.append(c)
      for n in c.set_of_nodes:
        self.lookup[n] = c.index
        self.all_nodes.append(n)

    # One instance method
  def move(self, node, new_community_index):
    self.list_of_communities[self.lookup[node]].remove_node(node)
    self.list_of_communities[new_community_index].add_node(node)
    self.lookup[node] = new_community_index

    # A second instance method
  def get_community_index(self, node):
    return self.lookup[node]

#need to do
  def get_community_from_node(self, node):
    return self.list_of_communities[self.lookup[node]]

  def number_of_nodes_in_partition(self):
    return len(self.lookup)
      
  def is_empty_partition(self):
    return len(self.lookup) == 0

  def get_nodes(self):
    return self.all_nodes

  def get_list_of_comm(self):
    return self.list_of_communities

slides = nx.Graph()
slides.add_edge(1,2)
slides.add_edge(1,3)
slides.add_edge(1,4)
slides.add_edge(1,7)
slides.add_edge(2,4)
slides.add_edge(2,3)
slides.add_edge(2,5)
slides.add_edge(3,5)
slides.add_edge(3,7)
slides.add_edge(3,8)
slides.add_edge(4,5)
slides.add_edge(5,6)
slides.add_edge(5,7)
slides.add_edge(6,8)
slides.add_edge(6,13)
slides.add_edge(7,8)
slides.add_edge(7,9)
slides.add_edge(7,10)
slides.add_edge(8,10)
slides.add_edge(8,13)
slides.add_edge(9,10)
slides.add_edge(9,11)
slides.add_edge(9,12)
slides.add_edge(10,11)
slides.add_edge(10,13)
slides.add_edge(11,12)
slides.add_edge(12,13)

m = slides.number_of_edges()
list_of_comm = []
one = Community(0,1)
one.add_node(3)
two = Community(1,2)
two.add_node(4)
two.add_node(5)
three = Community(2,6)
three.add_node(8)
three.add_node(13)
four = Community(3, 7)
five = Community(4, 9)
five.add_node(10)
five.add_node(11)
five.add_node(12)
list_of_comm.append(one)
list_of_comm.append(two)
list_of_comm.append(three)
list_of_comm.append(four)
list_of_comm.append(five)
print(slides.degree(7))
myP = Partition(list_of_comm,1)

list2 = []
inter = 0
for n in slides.nodes():
  list2.append(Community(inter, n))
  inter+=1
indepP = Partition(list2,2)

def print_communities(P):
  for c in P.list_of_communities:
    print(c.set_of_nodes)
  
print_communities(myP)
print_communities(indepP)

#GUARENTEED RIGHT DONT TOUCH

#adjacency matrix/actual # of edges
def adj(G, u,v):
  if G.has_edge(u,v) and u != v:
    return 1
  else:
    return 0
#expected # of edges k_v*k_u/2m
def exp(G,u,v):
  return G.degree(u)*G.degree(v)/(2*G.number_of_edges())
#kronecker delta
def kd(P, u,v):
  if P.get_community_index(u)== P.get_community_index(v):
    return 1
  else:
    return 0

def change_in_mod1(G, P,v,gamma):
  D = P.get_community_from_node(v)
  #nodes = [key for key,value in commy.items() if value == D] 
  con1 = -1/G.number_of_edges()
  summ1 = 0
  for u in D.set_of_nodes:
    summ1+=adj(G,u,v)
  expr1 = con1*summ1
  #second term
  con2 = (gamma* G.degree(v))/(2*G.number_of_edges()*G.number_of_edges())
  summ2 = 0
  for u in D.set_of_nodes:
    summ2+=slides.degree(u)
  expr2 = con2*summ2
  #print(str(expr1) + " + " + str(expr2))
  return expr1 + expr2

def change_in_mod2(G, P, v,C,gamma):
  con1 = 1/G.number_of_edges()
  summ1 = 0
  for u in C.set_of_nodes:
    summ1+=adj(G,u,v)
  expr1 = con1*summ1
  #second term
  con2 = (-1* gamma*G.degree(v))/(2*G.number_of_edges()*G.number_of_edges())
  summ2 = 0
  for u in C.set_of_nodes:
    summ2+=G.degree(u)
  expr2 = con2*summ2
  #print(str(expr1) + " + " + str(expr2))
  return expr1 + expr2

def mod_change(G, P, v,C,gamma=1):
  return change_in_mod1(G, P, v,gamma) + change_in_mod2(G, P, v,C,gamma)

#mod change works
print("mod change val: " + str(mod_change(slides,myP, 3,myP.get_community_from_node(5))))

#MODULARITY:
def modularity(G, P,gamma=1):
  nodes = P.get_nodes()
  #print(nodes)
  summation = 0
  for u in nodes:
    for v in nodes:
      diff = adj(G,u,v)-(gamma*exp(G,u,v))
      #print(str(u) + " " + str(v) + ", " + str(adj(G,u,v)) + " * " + str(kd(P,u,v)) )
      summation+=(diff*kd(P,u,v))
  return summation/(2*G.number_of_edges())

#C = {1,2,3}
#gets approx 0.7 - intended
print("mod val: " + str(modularity(slides, myP)))
print_communities(myP)

#actual number of edges in commy C, CPM E_c
def actual_number_of_edges_in_community(G,P,C_nodes):
  acc_num = 0
  for u in C_nodes:
    for v in C_nodes:
      acc_num+=(adj(G,u,v)*kd(P,u,v))
  return acc_num

def CPM(G, P, gamma=1):
  sum = 0
  for c in P:
    e_c = actual_number_of_edges_in_community(G,P,c.set_of_nodes)
    n_c = c.community_size()
    combi = math.factorial(n_c)/(math.factorial(2)*math.factorial(n_c-2))
    term = e_c-(gamma*combi)
    sum+=term
  return sum
def change_in_cpm1(G, P,v,gamma):
  D = P.get_community_from_node(v)
  nodes = set(D.set_of_nodes).difference(set({v}))
  special_c = actual_number_of_edges_in_community(G,P,nodes)
  s_n_c = D.community_size()-1
  #special_combi = math.factorial(s_n_c)/(math.factorial(2)*math.factorial(s_n_c-2))
  special_term = special_c-(gamma*1)
  return special_term

def change_in_cpm2(G, P, v,C,gamma):
  nodes = set(C.set_of_nodes).union(set({v}))
  special_c = actual_number_of_edges_in_community(G,P,nodes)
  s_n_c = C.community_size()+1
  special_combi = math.factorial(s_n_c)/(math.factorial(2)*math.factorial(s_n_c-2))
  special_term = special_c-(gamma*special_combi)
  return special_term

def cpm_change(G, P, v,C,gamma=1):
  return change_in_cpm1(G, P, v,gamma) + change_in_cpm2(G, P, v,C,gamma)

print(str(cpm_change(slides,myP, 7,myP.get_community_from_node(9))))

#pretty sure this is right
def move_nodes_fast(G, P, Q, v, gamma=1):
  print("in famove_nodes_fastst:")
  need_to_visit=set()
  curr_comm_index = P.get_community_index(v)
  #print("curr_comm_index" + str(curr_comm_index))
  neighbors = G.neighbors(v)
  v_neighbors = set(neighbors)
  #print("neighbors" + str([x for x in v_neighbors])) 
  best_neighbor = v
  best_val_change = 0
  for g in v_neighbors:
    #print("neighbor: " + str(g))
    mod_val = mod_change(G,P,v, P.get_community_from_node(g), gamma)
    #print("mod change val: " + str(mod_val))
    if mod_val > best_val_change:
      best_val_change = mod_val
      best_neighbor = g
  #print("best value_change: " + str(best_val_change))
  #print("best neighbor: " + str(best_neighbor))
  if best_val_change > 0:
    #print("move here")
    P.move(v, P.get_community_index(best_neighbor))
    #print("after move: ")
    #print_communities(P)
    neighbor_nodes = set(P.get_community_from_node(best_neighbor).set_of_nodes)
    N = v_neighbors.difference(neighbor_nodes)
    #print("N: " + str(N))
    need_to_visit = N.difference(Q)
    #print("need to visit: " + str(need_to_visit))
    Q.union(need_to_visit)
    #print("queue after addition: " + str(Q))
  return Partition(P.list_of_communities)

#WORKS
def SingletonPartition(G):
  commlist = []
  index = 0
  for n in G.nodes():
    tempcomm = Community(index, n)
    commlist.append(tempcomm)
    index+=1
  return Partition(commlist)
#print("singleton")
#print([c.set_of_nodes for c in SingletonPartition(slides).list_of_communities])

#GAMMA CHECKER WORKS
def edges_between_sets(G,A,B):
  result = []
  counter = 0
  for n1 in A:
    for n2 in G.adj[n1]:
      if n2 in B:      
        result.append((n1, n2))
        counter+=1
  return (counter,result)
#checkgamma connected
def CheckGammaConnected(G,T,S, gamma = 1):
  set_A = T
  set_B = S-T    
  c,r = edges_between_sets(G,set_A,set_B)
  val = 0.5 * len(set_A) * len(set_B)
  if c > val:
    return True
  else:
    return False 
#print(CheckGammaConnected(slides, set([1,2,3]), set([4,5,3]),0.5))

#DEF WORKS
def MergeNodesSubset(G,Partition,S_Community, gamma=1):
  print("entering MergeNodesSubset with Partition: " )
  R = []
  for n in S_Community.set_of_nodes:
    ownset = set([n])
    #print("current node: "  + str(ownset))
    if CheckGammaConnected(G,ownset,S_Community.set_of_nodes):
      R.append(n)
  #print("well connected nodes R: " + str(R))
  for v in R:
    tempset = set([v])
    #print("tempset: " + str(tempset))
    #print("Partition list of communities: " + str([c.set_of_nodes for c in Partition.list_of_communities]))
    P_set = [c.set_of_nodes for c in Partition.list_of_communities]
    if tempset in P_set:
      #print("singleton node " + str(v))
      #T is a list of communities
      T = []
      for c in Partition.list_of_communities:
        cnodes = c.set_of_nodes
        #print("cnodes: " + str(cnodes))
        if CheckGammaConnected(G,cnodes,S_Community.set_of_nodes) and cnodes.issubset(S_Community.set_of_nodes):
          T.append(c)
      #print("well connected communities: " + str([c.set_of_nodes for c in T]))
      #step 38:
      c_pr = []
      sum_probs = 0
      for c in T:
        modval =  mod_change(G,Partition, v, c) 
        #print("modval: " + str( mod_change(G,Partition, v, c)))
        if modval>= 0:
          c_pr.append(10*modval)
          sum_probs+=(10*modval)
        else:
          c_pr.append(0)
      i = 0
      c_weighted = []
      while i < len(c_pr):
        c_weighted.append(c_pr[i]/sum_probs)
        i+=1
      new_community = np.random.choice(T, p=c_weighted)
      old_community = Partition.get_community_from_node(v)
      old_community.remove_node(v)
      new_community.add_node(v)
      #print("new_community after adding:" + str(new_community.set_of_nodes))
  print("move_fast result:")
  print_communities(Partition)
  return Partition

#DEF WORKS
def RefinePartition(G, P,gamma=1):
  print("in refinedpartition:")
  P_refined = SingletonPartition(G)
  #print_communities(P_refined)
  #print([c.set_of_nodes for c in P_refined.list_of_communities])
  for C in P.list_of_communities:
    P_refined = MergeNodesSubset(G,P_refined,C)
    print("refined parititon loop: ")
    print([c.set_of_nodes for c in P_refined.list_of_communities])
  return P_refined

#LEIDEN FINALLY WORKS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!11
def leiden(G,P,gamma=1):
  Q = set(G.nodes())
  H_best = modularity(G,P,gamma)
  print("initial: " + str(H_best))
  print_communities(P)
  #P_copy = copy.deepcopy(P.list_of_communities)
  #returnP = Partition(P_copy)
  refinedP = P
  movefastP = P
  print(Q)
  while len(Q) != 0:
    P_copy = copy.deepcopy(movefastP.list_of_communities)
    returnP = Partition(P_copy)
    currentnode = Q.pop()
    #for v
    movefastP = move_nodes_fast(G,P,Q,currentnode)
    print_communities(movefastP)
    print("mod after movefast: " + str(modularity(G,movefastP)))
    if H_best - modularity(G,movefastP) > 0.001:
      return returnP
    else:
      H_best = modularity(G,movefastP)
      refinedP = RefinePartition(G,movefastP)
      print("move nodes result modularity: " + str(H_best))
      print_communities(refinedP)
  return P

res2 = leiden(slides,myP)
print("output: ")
print_communities(res2)

#GUARENTEED RIGHT DONT TOUCH
def move_nodes(G, P, v, gamma=1):
  curr_comm_index = P.get_community_index(v)
  neighbors = G.neighbors(v)
  best_neighbor = v
  best_val_change = 0
  for u in neighbors:
    curr_val = mod_change(G,P,v, P.get_community_from_node(u), gamma)
    if curr_val > best_val_change:
      best_val_change = curr_val
      best_neighbor = u
  if best_val_change > 0:
    P.move(v, P.get_community_index(best_neighbor))

def louvain(G,P,gamma=1):
  H_old = -0.5
  H_best = modularity(G,P,gamma)
  print("initial: " + str(H_best))
  print_communities(P)
  for v in G.nodes():
    P_copy = copy.deepcopy(P.list_of_communities)
    returnP = Partition(P_copy)
    move_nodes(G,P,v)
    if H_best - modularity(G,P) > 0.001:
      return returnP
    else:
      H_best = modularity(G,P,gamma)
      print("move nodes result: " + str(H_best))
      print_communities(P)
  return P
      
      #WORKS
res = louvain(slides,myP)
print_communities(res)

res2 = louvain(slides,indepP)
print_communities(res2)

#READ GRAPHS
protein_graph = nx.Graph()
for i,j in network.iterrows():
  protein_graph.add_edge(j[0],j[1])
network_community = {}

string_graph = nx.Graph()
for i,j in network.iterrows():
  string_graph.add_edge(j[0],j[1])
network_community = {}

for u,v in string_graph.edges():
  print(str(u) + " " + str(v))

Proteincomms = []
inter1 = 0
for n in protein_graph.nodes():
  Proteincomms.append(Community(inter1, n))
  inter1+=1
ProteinPartition = Partition(Proteincomms,2)

Stringcomms = []
inter2 = 0
for n in string_graph.nodes():
  Stringcomms.append(Community(inter2, n))
  inter2+=1
StringPartition = Partition(Stringcomms,3)

protein_louvain_gamma_small = louvain(protein_graph,ProteinPartition,0.1)
print_communities(protein_louvain_gamma_small)
string_louvain_gamma_small = louvain(string_graph,StringPartition,0.1)
print_communities(protein_louvain_gamma_small)



#Use Library to calculate values to check functions with:
import networkx.algorithms.community as nx_comm
list_of_sets = []
for c in ProteinPartition.list_of_communities:
  list_of_sets.append(c.set_of_nodes)
nx_comm.modularity(protein_graph, list_of_sets)