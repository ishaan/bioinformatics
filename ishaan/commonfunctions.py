#!/usr/bin/env python3
import pandas as pd
import networkx as nx
import math
import copy
import numpy as np
#Our Own Modules:
from classes import Community, Partition

def FindMaxLength(lst):
    maxLength = max(map(len, lst))
    return maxLength
  
def SingletonPartition(G):
  commlist = []
  index = 0
  for n in G.nodes():
    tempcomm = Community(index, n)
    commlist.append(tempcomm)
    index+=1
  return Partition(commlist)

def edge_set(G,A,B):         # 'in' operator is faster for sets
  result = []
  for n1 in A:           # Iterate through first set
    for n2 in G.adj[n1]: # Than through edges connected to a node
      if n2 in B:       # Check is it edge between A and B
        result.append((n1, n2))
  return result

def allSingleton(P):
  myres = True
  for c in P.list_of_communities:
    if len(c.set_of_nodes) > 1:
      return False
  return True

def print_communities(P):
  for c in P.list_of_communities:
    print("community index:" + str(c.index) + ", nodes: " + str(c.set_of_nodes))
    
#GAMMA CHECKER WORKS
def edges_between_sets(G,A,B):
  result = []
  counter = 0
  for n1 in A:
    for n2 in G.adj[n1]:
      if n2 in B:      
        result.append((n1, n2))
        counter+=1
  return (counter,result)