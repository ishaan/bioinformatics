#!/usr/bin/env python3
import pandas as pd
import networkx as nx
import math
import copy
import numpy as np
#Our Own Modules
import cpm
from modularity import modularity, mod_change
from classes import Community, Partition
from commonfunctions import FindMaxLength, allSingleton, SingletonPartition, edge_set, print_communities, edges_between_sets
'''
human_network = open('human_protein_network.txt', 'r')
network = pd.read_table(human_network)
string_network = open('STRING_network.txt', 'r')
network2 = pd.read_table(string_network)

#READ GRAPHS
protein = nx.Graph()
for i,j in network.iterrows():
  protein.add_edge(j[0],j[1])

protein_graph = nx.k_core(protein, 40)
'''
#checkgamma connected
def CheckGammaConnected(G,T,S, gamma = 1):
  set_A = T
  set_B = S-T
  c,r = edges_between_sets(G,set_A,set_B)
  val = 0.5 * len(set_A) * len(set_B)
  if c > val:
    return True
  else:
    return False

#DEF WORKS
def MergeNodesSubset(G,Partition,S_Community, gamma=1):
  print("entering MergeNodesSubset with Partition: " )
  R = []
  for n in S_Community.set_of_nodes:
    ownset = set([n])
    if CheckGammaConnected(G,ownset,S_Community.set_of_nodes):
      R.append(n)
  for v in R:
    tempset = set([v])
    P_set = [c.set_of_nodes for c in Partition.list_of_communities]
    if tempset in P_set:
      T = []
      for c in Partition.list_of_communities:
        cnodes = c.set_of_nodes
        #print("cnodes: " + str(cnodes))
        if CheckGammaConnected(G,cnodes,S_Community.set_of_nodes) and cnodes.issubset(S_Community.set_of_nodes):
          T.append(c)
      c_pr = []
      sum_probs = 0
      for c in T:
        modval =  mod_change(G,Partition, v, c)
        #print("modval: " + str( mod_change(G,Partition, v, c)))
        if modval>= 0:
          c_pr.append(10*modval)
          sum_probs+=(10*modval)
        else:
          c_pr.append(0)
      i = 0
      c_weighted = []
      if sum_probs == 0:
        new_community = T[0]
      else:
        while i < len(c_pr):
          c_weighted.append(c_pr[i]/sum_probs)
          i+=1
        new_community = np.random.choice(T, p=c_weighted)
      old_community = Partition.get_community_from_node(v)
      old_community.remove_node(v)
      new_community.add_node(v)
  print("move_fast result:")
  print_communities(Partition)
  return Partition

#DEF WORKS
def RefinePartition(G, P,gamma=1):
  print("in refinedpartition:")
  P_refined = SingletonPartition(G)
  for C in P.list_of_communities:
    P_refined = MergeNodesSubset(G,P_refined,C)
    print("refined parititon loop: ")
    print([c.set_of_nodes for c in P_refined.list_of_communities])
  return P_refined

#DEFINITELY WORKS - aggregate graph implementation/phase 2 addition:
def leiden_phase2(G,P,gamma=1):
  graphtemp = nx.Graph()
  E = set()
  for c in P.list_of_communities:
    for v in P.list_of_communities:
      myset = edge_set(G, c.set_of_nodes,v.set_of_nodes)
      wei = len(myset)
      if wei > 0:
        graphtemp.add_node(c.index, community=c)
        graphtemp.add_node(v.index, community=v)
        graphtemp.add_edge(c.index,v.index, weight=wei)
  return graphtemp

#MOVENODES FAST WORKS-----------------------------------------------
#pretty sure this is right
def move_nodes_fast(G, P, gamma=1):
  print("entered move_nodes_fast:")
  Q = set(G.nodes())
  print(Q)
  myP = Partition(P.list_of_communities)
  #H_best = modularity(G,myP,gamma)
  while len(Q) != 0:
    safeP = copy.deepcopy(myP.list_of_communities)
    returnP = Partition(safeP)
    currentnode = Q.pop()
    need_to_visit=set()
    curr_comm_index = myP.get_community_index(currentnode)
    #print("curr_comm_index" + str(curr_comm_index))
    neighbors = G.neighbors(currentnode)
    currentnode_neighbors = set(neighbors)
    best_neighbor = currentnode
    best_val_change = 0
    for g in currentnode_neighbors:
      mod_val = mod_change(G,myP,currentnode, myP.get_community_from_node(g), gamma)
      if mod_val > best_val_change:
        best_val_change = mod_val
        best_neighbor = g
    if best_val_change > 0:
      #print("move here")
      myP.move(currentnode, myP.get_community_index(best_neighbor))
      neighbor_nodes = set(myP.get_community_from_node(best_neighbor).set_of_nodes)
      N = currentnode_neighbors.difference(neighbor_nodes)
      #print("N: " + str(N))
      need_to_visit = N.difference(Q)
      #print("need to visit: " + str(need_to_visit))
      Q.union(need_to_visit)
  return myP

#line 8 in Leiden Pseudo - need to confirm Phase 2 (line 7 + 8):
def maintain_partition(G,P):
  list_of_new_communities = []
  #nodelist = list(G.nodes())
  for n,data in G.nodes(data='community'):
    #currentcomm = G.nodes[n]['community']
    currentnodes = data.set_of_nodes#currentcomm.set_of_nodes
    for C in P.list_of_communities:
      oldnodes = C.set_of_nodes
      if currentnodes.intersection(oldnodes) == currentnodes:
        list_of_new_communities.append(data)
  return Partition(list_of_new_communities)

#LEIDEN FINALLY WORKS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!11
def leiden(G,P,gamma=1):
  currG = G
  currP = P
  H_best = modularity(currG,currP,gamma)
  print("initial: " + str(H_best))
  print_communities(currP)
  refinedP = Partition(currP.list_of_communities) #initially just P
  currPar = move_nodes_fast(currG,currP)
  while True:
    if allSingleton(currPar):
      print("break here: ")
      break
    print("after getting rid of empty/not terminated: ")
    print_communities(currPar)
    refinedP = RefinePartition(currG,currPar)
    currG = leiden_phase2(currG,refinedP)
    currPar = maintain_partition(currG,currPar)
    print("new graph's node set: ")
    print(list(currG.nodes))
    print("new graph's edge set: ")
    print(list(currG.edges))
    print("new graph's partition: ")
    print_communities(currPar)
    print()
    currPar = move_nodes_fast(currG,currPar)
    print("afer this round of louvain_phase1:")
    print_communities(currPar)
  return currPar

'''
ProteinReady_leiden = SingletonPartition(protein_graph)
print_communities(ProteinReady_leiden)

res2 = leiden(protein_graph,ProteinReady_leiden)
print("output: ")
print_communities(res2)
'''