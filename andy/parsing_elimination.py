#!/usr/bin/env python3
import pandas as pd
import networkx as nx
import math
import copy #- original copy from Python Builtins
import numpy as np
#Our Own Modules:
from classes import Community, Partition
# Function for eliminating nodes from graph generated by reading edges of human_protein_network.
import networkx as nx

def print_communities(P):
  for c in P.list_of_communities:
    print(c.set_of_nodes)

def eliminate():
    '''
    Given large graph of connected nodes (read from human_protein_network), parse humanComplexes
    and generate communities. Keep track of all nodes encountered while parsing. Then, iterate
    through all nodes in the graph and delete those that were not read in humanComplexes.

    Change:
    g: networkx Graph constructed by parsing human_protein_network
    '''
    # Initialize list of nodes, list of Community objects, community index
    nodes_list = []
    coms = []
    #com_index = 0

    with open("andy/human_complexes_preprocessed.txt", "r") as f:
        # Skip first line with table headers
        f.readline()

        # Iterate through each line, split by tab
        line = f.readline().split("\t") # Get first line
        while line != [""]:
            com_index = line[0]
            # Get nodes in this community (line in file) - should be at least 1
            line_nodes = line[5].split(";")

            # For each line, build Community object
            for i in range(len(line_nodes)):
                curr = line_nodes[i]
                if i == 0:
                    # If at initial node, create Community object
                    com = Community(com_index, line_nodes[0])
                else:
                    # Add each node after initial node
                    com.add_node(curr)
            # Increment com_index, add Community object to coms
            #com_index += 1
            coms.append(com)
            line = f.readline().split("\t")
    return Partition(coms)

p = eliminate() # Generate Partition object - ground truth


# Write to same format as Comms_1, Comms_10
out = open("CORUM_partition.txt", "w")
idx = 1 # Initialize index at 1
for com in p.list_of_communities:

    # Build string - Community #:
    string = "Community " + str(idx) + ":"

    nodes_str = ""
    for node in com.get_nodes_in_community():
        # Get list of nodes separated by spaces
        nodes_str = nodes_str + " " + node

    # Add nodes list to main output string, add new line
    string = string + nodes_str + " \n"
    idx = idx + 1 # Increment

    # Write to file
    out.write(string)
out.close()

#print_communities(p)