#!/usr/bin/env python3
import pandas as pd
import networkx as nx
import math
import copy
import numpy as np
#Our Own Modules:
from classes import Community, Partition
#adjacency matrix/actual # of edges
def adj(G, u,v):
  if G.has_edge(u,v) and u != v:
    return 1
  else:
    return 0
#expected # of edges k_v*k_u/2m
def exp(G,u,v):
  return G.degree(u)*G.degree(v)/(2*G.number_of_edges())
#kronecker delta
def kd(P, u,v):
  if P.get_community_index(u)== P.get_community_index(v):
    return 1
  else:
    return 0

def change_in_mod1(G, P,v,gamma):
  D = P.get_community_from_node(v)
  #nodes = [key for key,value in commy.items() if value == D] 
  con1 = -1/G.number_of_edges()
  summ1 = 0
  for u in D.set_of_nodes:
    summ1+=adj(G,u,v)
  expr1 = con1*summ1
  #second term
  con2 = (gamma* G.degree(v))/(2*G.number_of_edges()*G.number_of_edges())
  summ2 = 0
  for u in D.set_of_nodes:
    summ2+=G.degree(u)
  expr2 = con2*summ2
  #print(str(expr1) + " + " + str(expr2))
  return expr1 + expr2

def change_in_mod2(G, P, v,C,gamma):
  con1 = 1/G.number_of_edges()
  summ1 = 0
  for u in C.set_of_nodes:
    summ1+=adj(G,u,v)
  expr1 = con1*summ1
  #second term
  con2 = (-1* gamma*G.degree(v))/(2*G.number_of_edges()*G.number_of_edges())
  summ2 = 0
  for u in C.set_of_nodes:
    summ2+=G.degree(u)
  expr2 = con2*summ2
  #print(str(expr1) + " + " + str(expr2))
  return expr1 + expr2

def mod_change(G, P, v,C,gamma=1):
  return change_in_mod1(G, P, v,gamma) + change_in_mod2(G, P, v,C,gamma)


#MODULARITY:
def modularity(G, P,gamma=1):
  nodes = P.get_nodes()
  #print(nodes)
  summation = 0
  for u in nodes:
    for v in nodes:
      diff = adj(G,u,v)-(gamma*exp(G,u,v))
      #print(str(u) + " " + str(v) + ", " + str(adj(G,u,v)) + " * " + str(kd(P,u,v)) )
      summation+=(diff*kd(P,u,v))
  return summation/(2*G.number_of_edges())

edges = [(10, 10), (10, 49), (10, 108), (10, 130), (10, 164), (10, 201), (49, 49), (49, 108), (49, 130), (108, 108), (108, 130), (108, 164), (108, 201), (130, 130), (130, 201), (164, 201)]
testgraph = nx.Graph()
for (u,v) in edges:
  testgraph.add_edge(u,v)

#Use Library to calculate values to check functions with:
#import networkx.algorithms.community as nx_comm
#list_of_sets = []
#for c in ProteinPartition.list_of_communities:
#  list_of_sets.append(c.set_of_nodes)
#nx_comm.modularity(protein_graph, list_of_sets)

listcommm = []
listcommm.append(Community(1, 10))
listcommm.append(Community(2,49))
listcommm.append(Community(3,108))
listcommm.append(Community(4,164))
listcommm.append(Community(5,130))
listcommm.append(Community(6,201))
party = Partition(listcommm)

print(modularity(testgraph,party))
  