#!/usr/bin/env python3
import pandas as pd
import networkx as nx
import math
import copy
import numpy as np
#Our Own Modules:
import cpm
from modularity import modularity, mod_change
from classes import Community, Partition
from commonfunctions import FindMaxLength, allSingleton, SingletonPartition, edge_set, print_communities

human_network = open('human_protein_network.txt', 'r')
network = pd.read_table(human_network)
#string_network = open('STRING_network.txt', 'r')
#network2 = pd.read_table(string_network)

#READ GRAPH
protein = nx.Graph()
for i,j in network.iterrows():
  protein.add_edge(j[0],j[1])

protein_graph = nx.k_core(protein, 20)

#GUARENTEED RIGHT DONT TOUCH
def move_nodes(G, P, v, gamma=1):
  curr_comm_index = P.get_community_index(v)
  neighbors = G.neighbors(v)
  best_neighbor = v
  best_val_change = 0
  for u in neighbors:
    curr_val = mod_change(G,P,v, P.get_community_from_node(u), gamma)
    if curr_val > best_val_change:
      best_val_change = curr_val
      best_neighbor = u
  if best_val_change > 0:
    P.move(v, P.get_community_index(best_neighbor))

def louvain_phase1(G,P,gamma=1):
  H_old = -0.5
  H_best = modularity(G,P,gamma)
  print("initial: " + str(H_best))
  for v in G.nodes():
    #P_copy = copy.deepcopy(P.list_of_communities)
    returnP = Partition(P.list_of_communities)
    move_nodes(G,P,v)
    if H_best - modularity(G,P) > 0.001:
      print("move nodes final result: " + str(H_best))
      return returnP
    else:
      H_best = modularity(G,P,gamma)
  return P

#aggregate graph implementation/phase 2:
def louvain_phase2(G,P,gamma=1):
  graphtemp = nx.Graph()
  E = set()
  for c in P.list_of_communities:
    for v in P.list_of_communities:
      myset = edge_set(G, c.set_of_nodes,v.set_of_nodes)
      wei = len(myset)
      if wei > 0:
        graphtemp.add_node(c.index, community=c)
        graphtemp.add_node(v.index, community=v)
        graphtemp.add_edge(c.index,v.index, weight=wei)
  return graphtemp

def louvain(G,P,gamma=1):
  holderG = G
  #P_copy = copy.deepcopy(P.list_of_communities)
  #returnP = Partition(P_copy)
  currPar = louvain_phase1(holderG,P)
  #print("result of inital louvain_phase1: ")
  #print_communities(currPar)
  while True:
    if allSingleton(currPar):
      print("break here: ")
      break
    holderG = louvain_phase2(holderG,currPar)
    currPar = SingletonPartition(holderG)
    currPar = louvain_phase1(holderG,currPar)
    #print("afer this round of louvain_phase1:")
    #print_communities(currPar)
  return P #debugging only, change P to returnP deepcopy-version

ProteinReady_walktrap = SingletonPartition(protein_graph)
#print_communities(ProteinReady_louvain)
#WORKS
res = louvain(protein_graph,ProteinReady_louvain)
print_communities(res)