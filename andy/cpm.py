#Ishaan to Sagar: can you double check these. I'm pretty sure it's correct I just want a second eye.
#actual number of edges in commy C, CPM E_c
def actual_number_of_edges_in_community(G,P,C_nodes):
  acc_num = 0
  for u in C_nodes:
    for v in C_nodes:
      acc_num+=(adj(G,u,v)*kd(P,u,v))
  return acc_num

def CPM(G, P, gamma=1):
  sum = 0
  for c in P:
    e_c = actual_number_of_edges_in_community(G,P,c.set_of_nodes)
    n_c = c.community_size()
    combi = math.factorial(n_c)/(math.factorial(2)*math.factorial(n_c-2))
    term = e_c-(gamma*combi)
    sum+=term
  return sum

def change_in_cpm1(G, P,v,gamma):
  D = P.get_community_from_node(v)
  nodes = set(D.set_of_nodes).difference(set({v}))
  special_c = actual_number_of_edges_in_community(G,P,nodes)
  s_n_c = D.community_size()-1
  #special_combi = math.factorial(s_n_c)/(math.factorial(2)*math.factorial(s_n_c-2))
  special_term = special_c-(gamma*1)
  return special_term

def change_in_cpm2(G, P, v,C,gamma):
  nodes = set(C.set_of_nodes).union(set({v}))
  special_c = actual_number_of_edges_in_community(G,P,nodes)
  s_n_c = C.community_size()+1
  special_combi = math.factorial(s_n_c)/(math.factorial(2)*math.factorial(s_n_c-2))
  special_term = special_c-(gamma*special_combi)
  return special_term

def cpm_change(G, P, v,C,gamma=1):
  return change_in_cpm1(G, P, v,gamma) + change_in_cpm2(G, P, v,C,gamma)
