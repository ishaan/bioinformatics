# Import required classes (e.g., List, Set, Map)

# A class representing a node in the network
class Node:
  # The node's identifier
  def __init__(self, id):
    self.id = id
    self.community = None

# A class representing a community in the network
class Community:
  # The community's identifier
  def __init__(self, id):
    self.id = id
    self.nodes = set()
    self.total_weight = 0

  # Add a node to the community
  def add_node(self, node):
    self.nodes.add(node)
    node.community = self.id

# The Leiden algorithm class
class Leiden:
  # The network as an adjacency list
  def __init__(self, adjacency_list):
    self.adjacency_list = adjacency_list
    self.communities = []

    # Initialize the communities
    community_id = 0
    for node in adjacency_list.keys():
      community = Community(community_id)
      community.add_node(node)
      self.communities.append(community)
      community_id += 1

  # Method to run the Leiden algorithm
  def run(self):
    # The algorithm continues until there is no improvement
    improvement = True
    while improvement:
      improvement = False

      # 1. Compute the total weight of each community
      for community in self.communities:
        community.total_weight = 0
        for node in community.nodes:
          for neighbor in self.adjacency_list[node]:
            if node.community == neighbor.community:
              community.total_weight += 1

      # 2. Compute the gain in modularity if each node moves to a different community
      max_gains = {}
      for node in self.adjacency_list.keys():
        max_gains[node] = 0
        for neighbor in self.adjacency_list[node]:
          if node.community != neighbor.community:
            current_community = self.communities[node.community]
            neighbor_community = self.communities[neighbor.community]
            dQ = (2 * neighbor_community.total_weight) / (current_community.total_weight + neighbor_community.total_weight) - (2 * current_community.total_weight) / (current_community.total_weight + neighbor_community.total_weight)
            if dQ > max_gains[node]:
              max_gains[node] = dQ
              max_gain_community = neighbor_community

      # 3. Move each node to the community with the maximum gain in modularity
      for node in self.adjacency_list.keys():
        if max_gains[node] > 0:
          improvement = True
          current_community = self.communities[node.community]
          current_community.nodes.remove(node)
          max_gain_community.add_node(node)

      # 4. Remove empty communities
      self.communities = [c for c in self.communities if len(c.nodes) > 0]


