## Code for parsing CORUM file


def read_CORUM(fname):

    with open(fname, "r") as f:

        # Skip first line with column names
        f.readline()

        line = f.readline().split("\t")
        hashMap = {}
        while line != [""]:
            # key is ID, values are ComplexName and subunitIDs
            hashMap[line[0]] = line[1], line[5].split(";")
            # Get next line to continue loop
            line = f.readline().split("\t")

    return hashMap

h = read_CORUM('humanComplexes.txt')
print(h['1']) #test
