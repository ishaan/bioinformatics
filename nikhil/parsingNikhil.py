## Code for parsing CORUM file
import networkx as nx

def read_CORUM(fname):

    with open(fname, "r") as f:

        # Skip first line with column names
        f.readline()

        line = f.readline().split("\t")
        hashMap = {}
        while line != [""]:
            # key is ID, values are ComplexName and subunitIDs
            hashMap[line[0]] = line[1], line[5].split(";")
            # Get next line to continue loop
            line = f.readline().split("\t")

    return hashMap

h = read_CORUM('humanComplexes.txt')





#add edges from human_protein_network.txt into a graph 
def readProteinNetwork(fname):

    g = nx.DiGraph()
    with open(fname, "r") as f:

        f.readline()
        line = f.readline().split("\t")

        while line != None:
            line = f.readline().split("\t")
            print(line[2])
            # g.add_edge(line[0], line[1], weight = line[2])
        
    return g

y = readProteinNetwork('human_protein_network.txt')
print(y)